sudo yum -y install wget
cd /tmp
sudo wget https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
sudo wget http://rpms.remirepo.net/enterprise/remi-release-7.rpm
sudo rpm -Uvh *.rpm
sudo yum -y update
sudo rm -rf *.rpm

# setup timezone and ntp time
sudo timedatectl set-timezone America/Los_Angeles
sudo yum -y install ntp
sudo systemctl start ntpd
sudo systemctl enable ntpd

sudo yum -y install yum-utils
sudo yum -y update

# apache
sudo yum -y install httpd
sudo systemctl start httpd
sudo systemctl enable httpd
sudo yum -y install mod_ssl

#mysql
sudo yum -y install mariadb mariadb-server mariadb-devel
sudo systemctl start mariadb
sudo systemctl enable mariadb

# php 7.1
sudo yum-config-manager --enable remi-php71
sudo yum -y install php php-mysql php-pdo php-common php-mcrypt php-gd php-devel php-pear php-curl php-mbstring php-soap php-xml php-zip

# SELINUX disable on next boot
sudo sed -ie 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config
# SELINUX set to not enforce temporarily - until next boot then we disable above
sudo setenforce 0

ln -fs /home/vagrant/sync /var/www/vpc-training

ln -fs /var/www/vpc-training/config/httpd.vpctraining.conf /etc/httpd/conf.d/
sudo sed -ie 's/User apache/User vagrant/g' /etc/httpd/conf/httpd.conf

sudo mysqladmin -u root password root
mysql -uroot -p'root' -e "grant all privileges on *.* to 'root'@'%' identified by 'root'";
mysql -uroot -p'root' -e "grant all privileges on *.* to 'root'@'localhost' identified by 'root'";
mysql -uroot -p'root' -e "create database vpc_training"

#composer
cd /tmp
curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer

cd /home/vagrant/sync
composer install
composer dumpauto
cp .env.example .env

php artisan key:generate
php artisan migrate